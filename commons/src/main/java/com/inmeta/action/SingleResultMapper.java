package com.inmeta.action;


import com.inmeta.fsm.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Vaagan
 */
public abstract class SingleResultMapper<E, T> {
    private List<String> errors = new ArrayList<String>();

    public abstract SingleResultMapper<E, T> build(final E entity);


    public abstract T getResult();

    public final SingleResultMapper<E, T> addEerror(final String error) {
        errors.add(error);
        return this;
    }

    public final List<String> getErrors() {
        return errors;
    }

    public void addErrors(final Errors errors) {
        for (com.inmeta.fsm.Error error : errors.getErrors()) {
            this.errors.add(String.format("[%s] - %s", error.getSource(), error.getMessage()));
        }
    }

    public boolean hasError() {
        return errors.size() > 0;
    }
}
