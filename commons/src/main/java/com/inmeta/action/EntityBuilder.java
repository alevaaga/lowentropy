package com.inmeta.action;

/**
 * @author Alexander Vaagan
 */
public interface EntityBuilder<T> {

    public T get();
}
