package com.inmeta.action;

import com.inmeta.fsm.*;
import com.inmeta.fsm.Error;

/**
 * @author Alexander Vaagan
 */
public abstract class Action<E> {
    private final Errors errors = new Errors();

    private EventDispatcher<?, E> eventSystem;

    public abstract E execute(final E entity);

    public final Action<E> setEventSystem(EventDispatcher<?, E> eventSystem) {
        this.eventSystem = eventSystem;
        return this;
    }


    protected final InvocationResult<E> process(E entity, Event event) {
        InvocationResult<E> result = eventSystem.process(entity, event);
        addErrors(result.getErrors());

        return result;
    }


    protected final void addError(Error error) {
        errors.add(error);
    }

    protected final void addErrors(Errors errors) {
        for (com.inmeta.fsm.Error error : errors.getErrors()) {
            addError(error);
        }
    }

    public final Errors getErrors() {
        return errors;
    }
}
