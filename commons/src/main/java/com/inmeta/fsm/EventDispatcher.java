package com.inmeta.fsm;

/**
 * @param <S> State identifier type
 * @param <T> Target type
 * @author Alexander Vaagan
 */
public class EventDispatcher<S, T> {

    private StateMachine<S, T> stateMachine;

    /**
     * Constructor.
     *
     * @param stateMachine Top level state machine
     */
    public EventDispatcher(final StateMachine<S, T> stateMachine) {
        this.stateMachine = stateMachine;
    }

    /**
     * Process event.
     *
     * @param request target entity
     * @param event   Event
     * @return Result
     */
    public InvocationResult<T> process(final T request, final Event event) {
        InvocationContext<S, T> context = stateMachine.process(request, event);
        while (context.hasQueuedEvent()) {
            final T target = context.getResult().getTarget();
            final Event evt = context.popEvent();
            stateMachine.process(target, evt, context);
        }

        return context.getResult();
    }

    /**
     * Process event.
     * This method will create a new target instance before handling the event.
     *
     * @param event Event
     * @return Result
     */
    public InvocationResult<T> process(final Factory<T> factory, final Event event) {
        InvocationContext<S, T> context = stateMachine.process(factory, event);
        if (context.hasQueuedEvent()) {
            final T target = context.getResult().getTarget();
            final Event evt = context.popEvent();
            stateMachine.process(target, evt, context);

        }

        return context.getResult();
    }
}
