package com.inmeta.fsm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @param <S> State identifier type.
 * @param <T> Target type
 * @author Alexander Vaagan
 */
public class State<S, T> {
    private List<Guard<T>> guards = new ArrayList<Guard<T>>();
    private List<GuardedAction> entryActions = new ArrayList<GuardedAction>();
    private Map<String, Transition> transitions = new HashMap<String, Transition>();

    protected S state;

    /**
     * Construct a new state with given identifier.
     *
     * @param state State identifier
     */
    public State(final S state) {
        this.state = state;
    }

    public S getState() {
        return state;
    }

    /**
     * Add guard.
     *
     * @param guard Guard
     * @return this
     */
    public final State<S, T> addGuard(final Guard<T> guard) {
        guards.add(guard);
        return this;
    }

    /**
     * Add entry handler.
     *
     * @param handler Handler
     * @return this
     */
    public final State<S, T> addEntryAction(final StateHandler<T> handler) {
        entryActions.add(new GuardedAction(null, handler));
        return this;
    }

    /**
     * Add guarded handler.
     * The handler will only be executed if the guard evaluates to true.
     *
     * @param guard  Guard
     * @param action Handler
     * @return this
     */
    public State<S, T> addEntryAction(final Guard<T> guard, final StateHandler<T> action) {
        entryActions.add(new GuardedAction(guard, action));
        return this;
    }

    /**
     * Add transition to target state by given event.
     *
     * @param event  Event
     * @param target Target state
     */
    public void addTransition(final Enum<?> event, final State<S, T> target) {
        addTransition(event, target, null);
    }

    /**
     * Add transition to target state by given event.
     * Before making the transition, the given handler is invoked.
     *
     * @param event   Event Event
     * @param target  Target state Target
     * @param handler Transition handler
     */
    public void addTransition(final Enum<?> event, final State<S, T> target, final TransitionHandler<T> handler) {
        transitions.put(event.toString(), new Transition(event, target, handler));
    }


    void entryActions(final DependencyResolver resolver,
                      final T subject,
                      final InvocationContext<S, T> context) {

        InvocationResult result = context.getResult();
        context.logger().stateHandler();
        T target = subject;
        for (GuardedAction entryAction : entryActions) {
            Guard<T> guard = entryAction.guard;
            StateHandler<T> action = entryAction.action;
            if (null != guard) {
                context.logger().guards().logln(guard.getName());
                if (guard.setResolver(resolver).isValid(target)) {
                    context.logger().success().end();

                    context.logger().log(action.getName());
                    try {
                        target = action.init(resolver, context).apply(target);
                        context.logger().success();
                    } catch (final RuntimeException e) {
                        context.logger().error(action.getErrors());
                        throw e;
                    } finally {
                        context.logger().end();
                    }
                } else {
                    result.getErrors().add(guard.getErrors());
                    context.logger().error(guard.getErrors()).end();
                }
            } else {
                context.logger().logln(action.getName());
                try {
                    target = action.init(resolver, context).apply(target);
                    context.logger().success();
                } catch (final RuntimeException e) {
                    context.logger().error(action.getErrors());
                    throw e;
                } finally {
                    context.logger().end();
                }
            }

            if (null != action.getQueuedEvent()) {
                context.queueEvent(action.getQueuedEvent());
                context.logger().queueEvent(action.getQueuedEvent());
            }
        }
    }

    void checkGuards(final DependencyResolver resolver, final T subject, final InvocationContext<S, T> context) {
        final InvocationResult result = context.getResult();
        context.logger().guards();
        boolean valid = true;
        for (Guard<T> guard : guards) {
            context.logger().log(guard.getName());
            guard.setResolver(resolver);
            if (!guard.isValid(subject)) {
                valid = false;
                result.getErrors().add(guard.getErrors());
                context.logger().error(guard.getErrors());
            } else {
                context.logger().success();
            }
        }
        context.setGuardState(valid ? GuardState.PASSED : GuardState.FAILED);
        context.logger().end();
    }

    Transition findTransition(final Event event) {
        return transitions.get(event.getEvent().toString());
    }


    T handle(final T target, final Event event, final DependencyResolver resolver, final InvocationContext<S, T> context) {
        StateAccessor<S, T> stateAccessor = context.createStateAccessor();
        S stateIdentifier = stateAccessor.getState(target);
        final State<S, T>.Transition transition = findTransition(event);
        if (null == transition) {
            context.logger()
                    .error("No transition found from state %s by event %s.",
                            stateIdentifier.toString(), event.getEvent().toString())
                    .end();
            throw new NoSuchTransitionException(stateIdentifier.toString(), event.getEvent().toString());
        }

        context.logger().logln(
                String.format("Transitioning to state %s from state %s by event %s",
                        transition.getTarget().toString(),
                        stateIdentifier.toString(),
                        event.toString())
        );

        T resultTarget = target;
        final TransitionHandler<T> transitionCommand = transition.getCommand();
        if (null != transitionCommand) {
            context.logger().transitionHandler().log(transitionCommand.getName());
            transitionCommand.init(resolver, context);
            try {
                resultTarget = transitionCommand.apply(event, resultTarget);
                context.logger().success();
            } catch (Exception e) {
                context.logger().error(transitionCommand.getErrors());
            } finally {
                context.logger().end();
            }
        }

        State<S, T> targetState = transition.getTarget();
        targetState.checkGuards(resolver, resultTarget, context);
        if (context.guardsPassed()) {
            targetState.entryActions(resolver, resultTarget, context);
        }

        if (context.guardsPassed() && !context.getResult().error()) {
            stateAccessor.setState(targetState.getState(), resultTarget);
        }

        return resultTarget;
    }

    private class GuardedAction {
        private final Guard<T> guard;
        private final StateHandler<T> action;

        private GuardedAction(final Guard<T> guard, final StateHandler<T> action) {
            this.guard = guard;
            this.action = action;
        }
    }

    class Transition {
        private Enum<?> event;
        private State<S, T> target;
        private TransitionHandler<T> command;

        public Transition(final Enum<?> event, final State<S, T> state, final TransitionHandler<T> command) {
            this.event = event;
            this.target = state;
            this.command = command;
        }

        Enum<?> getEvent() {
            return this.event;
        }

        State<S, T> getTarget() {
            return this.target;
        }

        TransitionHandler<T> getCommand() {
            return this.command;
        }

        @Override
        public String toString() {
            return String.format("%s => %s", event.toString(), target.toString());
        }

        @Override
        public int hashCode() {
            return event.hashCode();
        }

        @Override
        public boolean equals(final Object obj) {
            if ((null == obj) || !(obj instanceof State.Transition)) {
                return false;
            }
            Transition transition = (Transition) obj;
            return event.equals(transition.event);
        }
    }

    @Override
    public String toString() {
        return state.toString();
    }
}
