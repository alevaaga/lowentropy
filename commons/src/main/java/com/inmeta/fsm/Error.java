package com.inmeta.fsm;

/**
 * @author Alexander Vaagan
 */
public class Error {
    private String source;
    private String message;
    private Throwable throwable;

    private Error() {
    }

    public String getSource() {
        return source;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    /**
     * Create builder.
     *
     * @return Builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Error builder.
     */
    public static class Builder {
        private Error Error = new Error();

        Builder() {
        }

        /**
         * Set source component.
         *
         * @param sourceComponent Source component
         * @return Builder
         */
        public Builder sourceComponent(final String sourceComponent) {
            Error.source = sourceComponent;
            return this;
        }

        /**
         * Set message.
         *
         * @param message Message
         * @return this
         */
        public Builder message(final String message) {
            Error.message = message;
            return this;
        }

        /**
         * Set throwable.
         *
         * @param t Throwable
         * @return Builder
         */
        public Builder throwable(final Throwable t) {
            Error.throwable = t;
            return this;
        }

        /**
         * Build error.
         *
         * @return Error
         */
        public Error build() {
            return Error;
        }
    }
}
