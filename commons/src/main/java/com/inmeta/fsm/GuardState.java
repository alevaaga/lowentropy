package com.inmeta.fsm;

/**
 * @author Alexander Vaagan
 */
public enum GuardState {
    /**
     * Passed.
     */
    PASSED,

    /**
     * Failed.
     */
    FAILED
}
