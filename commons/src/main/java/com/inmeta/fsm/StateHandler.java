package com.inmeta.fsm;

/**
 * @param <T> Target type
 * @author Alexander Vaagan
 */
public abstract class StateHandler<T> implements DependencyResolver {
    private InvocationContext context;
    private DependencyResolver resolver;
    private Event queuedEvent;
    private Errors errors = new Errors();



    /**
     * Init.
     * This method is called just prior to invocation.
     * @param resolver Dependency resolver
     * @param context Invocation context
     * @return This
     */
    public StateHandler<T> init(final DependencyResolver resolver, final InvocationContext context) {
        this.resolver = resolver;
        this.context = context;
        init();
        return this;
    }

    /**
     * Override this to to further initializations.
     */
    protected void init() {

    }

    protected void queueEvent(final Event queuedEvent) {
        this.queuedEvent = queuedEvent;
    }

    protected Event getQueuedEvent() {
        return queuedEvent;
    }

    protected StateHandler<T> addError(final Error Error) {
        this.errors.add(Error);
        return this;
    }

    protected StateHandler<T> addError(final Error.Builder builder) {
        errors.add(builder.build());
        return this;
    }


    protected StateHandler<T> addError(final String sourceComponent, final String message, final Throwable t) {
        errors.add(Error.builder()
                .sourceComponent(sourceComponent)
                .message(message)
                .throwable(t)
                .build());

        return this;
    }

    public Errors getErrors() {
        return errors;
    }


    @Override
    public <B> B getBean(final Class<B> tClass) {
        return resolver.getBean(tClass);
    }

    @Override
    public <B> B getBean(final String descriminator, final Class<B> tClass) {
        return resolver.getBean(descriminator, tClass);
    }

    /**
     * Apply command.
     *
     * @param subject The subject
     * @return subject
     */
    public abstract T apply(final T subject);

    /**
     * Get name of handler.
     *
     * @return Name
     */
    public String getName() {
        return getClass().getSimpleName();
    }

}
