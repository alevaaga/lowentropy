package com.inmeta.fsm;

import com.inmeta.fsm.log.Logger;
import com.inmeta.fsm.log.LoggerFactory;
import com.inmeta.fsm.log.TransitionLog;

/**
 * @param <S> State type.
 * @param <T> Target type.
 * @author Alexander Vaagan
 */
public abstract class InvocationContext<S, T> {
    private GuardState guardState = GuardState.PASSED;
    private InvocationResult<T> result = new InvocationResult<T>();
    private Event event;
    private LoggerFactory loggerFactory = new LoggerFactory();
    private Logger log;

    /**
     * Constructor.
     */
    public InvocationContext() {
        log = loggerFactory.create();
    }

    /**
     * Get invocation result.
     *
     * @return InvocationResult
     */
    public final InvocationResult<T> getResult() {
        return result;
    }

    /**
     * Create new state accessor.
     *
     * @return StateAccessor instance
     */
    public abstract StateAccessor<S, T> createStateAccessor();

    /**
     * Create logger.
     * @return TransitionLog
     */
    public Logger logger() {
        return log;
    }

    final InvocationContext queueEvent(final Event event) {
        this.event = event;
        return this;
    }

    final Event popEvent() {
        Event evt = this.event;
        this.event = null;
        return evt;
    }

    final boolean hasQueuedEvent() {
        return null != event;
    }

    void setGuardState(final GuardState guardState) {
        this.guardState = guardState;
    }

    boolean guardsPassed() {
        return guardState == GuardState.PASSED;
    }


}
