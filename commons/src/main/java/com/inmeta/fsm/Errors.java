package com.inmeta.fsm;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Vaagan
 */
public class Errors {
    private List<Error> Errors = new ArrayList<Error>();

    /**
     * Add errors.
     *
     * @param errors Errors
     * @return this
     */
    public Errors add(final Errors errors) {
        this.Errors.addAll(errors.getErrors());
        return this;
    }

    /**
     * Get errors.
     *
     * @return list of errors
     */
    public List<Error> getErrors() {
        return Errors;
    }

    /**
     * Add error.
     *
     * @param Error Error
     */
    public void add(final Error Error) {
        Errors.add(Error);
    }

    /**
     * Determine if there are any errors.
     * @return true / false
     */
    public boolean success() {
        return Errors.size() == 0;
    }

    @Override
    public String toString() {
        if(Errors.size() == 0) {
            return "No errors.";
        } else {
            return String.format("%d errors", Errors.size());
        }
    }
}
