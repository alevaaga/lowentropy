package com.inmeta.fsm;

/**
 * @param <E> Event identifier type.
 * @author Alexander Vaagan
 */
public class Event<E> {
    private E event;

    /**
     * Constructor.
     *
     * @param event Event identifier
     */
    public Event(final E event) {
        this.event = event;
    }

    /**
     * Get event identifier.
     *
     * @return Event identifier
     */
    public E getEvent() {
        return event;
    }

    @Override
    public String toString() {
        return event.toString();
    }
}
