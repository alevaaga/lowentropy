package com.inmeta.fsm;

/**
 * @param <S> State type
 * @param <T> Target type
 * @author Alexander Vaagan
 */
public class CompositeState<S, T> extends State<S, T> {
    private final StateMachine<?, T> stateMachine;

    /**
     * Constructor.
     *
     * @param state        State identifier
     * @param stateMachine Sub state machine
     */
    public CompositeState(final S state, final StateMachine<?, T> stateMachine) {
        super(state);
        this.stateMachine = stateMachine;
    }

    @Override
    T handle(final T target, final Event event, final DependencyResolver resolver, final InvocationContext<S, T> context) {
        Transition transition = findTransition(event);
        if (null != transition) {
            super.handle(target, event, resolver, context);
        } else {
            InvocationContext<?, T> subContext = stateMachine.process(target, event);
            context.getResult().getErrors().add(subContext.getResult().getErrors());
            context.queueEvent(subContext.popEvent());
        }
        return target;
    }


    @Override
    void entryActions(final DependencyResolver resolver, final T target, final InvocationContext<S, T> context) {
        super.entryActions(resolver, target, context);
        InvocationContext<?, T> subContext = stateMachine.entryActions(resolver, target);
        context.getResult().getErrors().add(subContext.getResult().getErrors());
    }

    @Override
    void checkGuards(final DependencyResolver resolver, final T target, final InvocationContext<S, T> context) {
        super.checkGuards(resolver, target, context);
        InvocationContext<?, T> subContext = stateMachine.checkGuards(resolver, target);
        context.getResult().getErrors().add(subContext.getResult().getErrors());
    }
}
