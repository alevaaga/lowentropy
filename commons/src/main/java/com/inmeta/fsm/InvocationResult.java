package com.inmeta.fsm;

/**
 * @param <T> Target type
 * @author Alexander Vaagan
 */
public class InvocationResult<T> {
    private Errors errors = new Errors();
    private T target;

    public void setTarget(final T target) {
        this.target = target;
    }

    public T getTarget() {
        return target;
    }

    public Errors getErrors() {
        return errors;
    }

    /**
     * Determine if the invocation was a success.
     * @return true / false
     */
    public boolean success() {
        return errors.success();
    }

    /**
     * Determine if the invocation produced errors.
     * @return true / false
     */
    public boolean error() {
        return !errors.success();
    }
}
