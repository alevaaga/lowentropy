package com.inmeta.fsm;

/**
 * @author Alexander Vaagan
 */
public interface InvocationContextFactory<S, T> {

    /**
     * Create new invocation context instance.
     *
     * @return InvocationContext
     */
    InvocationContext<S, T> createContext();
}
