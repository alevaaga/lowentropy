package com.inmeta.fsm;

import com.inmeta.fsm.log.Logger;
import com.inmeta.fsm.log.TransitionLog;

/**
 * @param <S> State type
 * @param <T> Target type
 * @author Alexander Vaagan
 */
public class StateMachine<S, T> {
    private StateMap<S, T> stateMap;
    private DependencyResolver resolver;
    private InvocationContextFactory<S, T> instanceFactory;

    /**
     * Constructor.
     *
     * @param resolver        Dependency resolver
     * @param instanceFactory Instance factory
     */
    public StateMachine(final DependencyResolver resolver, final InvocationContextFactory<S, T> instanceFactory) {
        this.resolver = resolver;
        this.instanceFactory = instanceFactory;
    }

    /**
     * Set state map.
     *
     * @param stateMap StateMap
     * @return this
     */
    public StateMachine<S, T> setMap(final StateMap<S, T> stateMap) {
        this.stateMap = stateMap;
        return this;
    }

    InvocationContext<S, T> process(final Factory<T> target, final Event event) {
        return process(target, event);
    }

    InvocationContext<S, T> process(final T subject, final Event event) {
        return process(subject, event, instanceFactory.createContext());
    }

    InvocationContext<S, T> process(final T target, final Event event, final InvocationContext<S, T> context) {
        T resultTarget;
        S stateIdentifier = context.createStateAccessor().getState(target);
        State<S, T> currentState = stateMap.findState(stateIdentifier);

        context.logger().startTransition();
        resultTarget = currentState.handle(target, event, resolver, context);

        context.getResult().setTarget(resultTarget);
        context.logger().end();

        return context;
    }

    InvocationContext<S, T> checkGuards(final DependencyResolver resolver, final T target) {
        InvocationContext<S, T> subContext = instanceFactory.createContext();
        StateAccessor<S, T> stateAccessor = subContext.createStateAccessor();
        S stateIdentifier = stateAccessor.getState(target);
        State<S, T> currentState = stateMap.findState(stateIdentifier);

        currentState.checkGuards(resolver, target, subContext);

        return subContext;
    }

    InvocationContext<S, T> entryActions(final DependencyResolver resolver, final T target) {
        InvocationContext<S, T> subContext = instanceFactory.createContext();
        StateAccessor<S, T> stateAccessor = subContext.createStateAccessor();
        S stateIdentifier = stateAccessor.getState(target);
        State<S, T> currentState = stateMap.findState(stateIdentifier);

        currentState.entryActions(resolver, target, subContext);

        return subContext;
    }

}
