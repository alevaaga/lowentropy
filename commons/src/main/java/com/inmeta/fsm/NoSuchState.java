package com.inmeta.fsm;

/**
 * @author Alexander Vaagan
 */
public class NoSuchState extends RuntimeException {

    /**
     * Constructor.
     * @param state State
     */
    public NoSuchState(final String state) {
        super("No such state found: " + state);
    }
}
