package com.inmeta.fsm.log;

import com.inmeta.fsm.*;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Alexander Vaagan
 */
public class SimpleLogger implements Logger {
    private String SEPARATOR = "   ";

    private PrintStream out;
    private int indent = 0;

    public SimpleLogger() {
        out = System.out;
    }

    @Override
    public Logger start() {
        indent++;
        return this;
    }

    @Override
    public Logger transitionHandler() {
        logln("Executing trnsition handlers.");
        return start();
    }

    @Override
    public Logger guards() {
        logln("Checking guards.");
        return start();
    }

    @Override
    public Logger stateHandler() {
        logln("Executing state handlers.");
        return start();
    }

    @Override
    public Logger startTransition() {
        logln("Starting transition at %s.", new SimpleDateFormat("dd.MM.yyyy HH:MM:ss").format(new Date()));
        return start();
    }

    @Override
    public Logger queueEvent(final Event queuedEvent) {
        return logln("Queueing event " + queuedEvent.getEvent().toString());
    }

    @Override
    public Logger logln(final String format, final String... args) {
        logln(String.format(format, args));
        return this;
    }

    @Override
    public Logger logln(final String message) {
        out.println(indent() + message);
        return this;
    }

    @Override
    public Logger log(final String message) {
        out.print(indent() + message);
        return this;
    }

    @Override
    public Logger end() {
        indent--;
        logln("Finished!").logln("\n");
        return this;
    }

    @Override
    public Logger error(final Errors errors) {
        logln("[ERROR]");
        for (com.inmeta.fsm.Error error : errors.getErrors()) {
            log("[Err]"); logln(error.getMessage());
        }

        return this;
    }

    @Override
    public Logger error(final String messageFormat, final String... args) {
        logln(String.format(messageFormat, args));
        return this;
    }

    @Override
    public Logger success() {
        return logln("......[OK!]");
    }

    private String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }

    private String padLeft(String s, int n) {
        return String.format("%1$" + n + "s", s);
    }


    private String indent() {
        String res = "";
        for (int i = 0; i < indent; i++) {
            res += SEPARATOR;
        }
        return res;
    }

}
