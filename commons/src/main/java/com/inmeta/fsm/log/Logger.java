package com.inmeta.fsm.log;

import com.inmeta.fsm.Errors;
import com.inmeta.fsm.Event;

/**
 * @author Alexander Vaagan
 */
public interface Logger {

    Logger start();

    Logger transitionHandler();

    Logger guards();

    Logger stateHandler();

    Logger startTransition();

    Logger logln(final String message);

    Logger log(final String message);

    Logger end();

    Logger error(final Errors errors);


    Logger success();

    Logger error(final String messageFormat, final String... args);

    Logger logln(String format, String... args);

    Logger queueEvent(final Event queuedEvent);
}
