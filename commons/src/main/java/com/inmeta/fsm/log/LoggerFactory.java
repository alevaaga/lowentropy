package com.inmeta.fsm.log;

/**
 * @author Alexander Vaagan
 */
public class LoggerFactory {

    public Logger create() {
        return new SimpleLogger();
    }
}
