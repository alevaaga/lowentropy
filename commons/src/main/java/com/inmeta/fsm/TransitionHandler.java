package com.inmeta.fsm;

/**
 * @param <T> Target type
 * @author Alexander Vaagan
 */
public abstract class TransitionHandler<T> implements DependencyResolver {
    private InvocationContext context;
    private DependencyResolver resolver;

    /**
     * Init.
     *
     * @param resolver Dependency resolver
     * @param result   Invocation result
     * @return this
     */
    public final TransitionHandler init(final DependencyResolver resolver, final InvocationContext result) {
        this.resolver = resolver;
        this.context = result;
        init();
        return this;
    }

    /**
     * Override this method to to custom initializations like bean lookup.
     */
    protected void init() {

    }

    protected DependencyResolver getResolver() {
        return resolver;
    }

    @Override
    public final <B> B getBean(final Class<B> tClass) {
        return resolver.getBean(tClass);
    }

    @Override
    public final <B> B getBean(final String descriminator, final Class<B> tClass) {
        return resolver.getBean(descriminator, tClass);
    }

    protected final TransitionHandler<T> addError(final Error.Builder builder) {
        context.getResult().getErrors().add(builder.build());
        return this;
    }

    protected final TransitionHandler<T> addError(final String sourceComponent, final String message, final Throwable t) {
        context.getResult().getErrors().add(Error.builder()
                .sourceComponent(sourceComponent)
                .message(message)
                .throwable(t)
                .build());

        return this;
    }

    public final Errors getErrors() {
        return context.getResult().getErrors();
    }


    /**
     * Apply command.
     *
     * @param event   The event
     * @param subject The subject
     * @return subject
     */
    public abstract T apply(final Event event, final T subject);

    /**
     * Get name of handler.
     *
     * @return Name
     */
    public String getName() {
        return getClass().getSimpleName();
    }
}
