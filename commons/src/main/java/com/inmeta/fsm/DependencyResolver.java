package com.inmeta.fsm;

/**
 * @author Alexander Vaagan
 */
public interface DependencyResolver {

    /**
     * Get bean by type.
     * @param tClass Type
     * @param <T> type
     * @return bean
     */
    <T> T getBean(final Class<T> tClass);

    /**
     * Get bean by discriminator and type.
     * @param descriminator Discriminator
     * @param tClass Type
     * @param <T> type
     * @return bean
     */
    <T> T getBean(String descriminator, Class<T> tClass);
}
