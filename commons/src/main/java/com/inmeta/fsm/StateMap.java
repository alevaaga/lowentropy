package com.inmeta.fsm;

import java.util.ArrayList;
import java.util.List;

/**
 * State map super class.
 * Implementations of this class should configure the state map.
 *
 * @param <S> State type
 * @param <T> Target type
 * @author Alexander Vaagan
 */
public abstract class StateMap<S, T> {
    private List<State<S, T>> states = new ArrayList<State<S, T>>();

    /**
     * Add state.
     *
     * @param state The state
     * @return this
     */
    public State<S, T> addState(final State<S, T> state) {
        states.add(state);
        return state;
    }

    /**
     * Find a state by its identifier.
     *
     * @param state Identifier
     * @return State
     */
    public State<S, T> findState(final S state) {
        for (State<S, T> s : states) {
            if (s.state.equals(state)) {
                return s;
            }
        }
        throw new NoSuchState(state.toString());
    }

}
