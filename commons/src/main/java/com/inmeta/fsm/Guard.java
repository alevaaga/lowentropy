package com.inmeta.fsm;

/**
 * @param <T> Target type
 * @author Alexander Vaagan
 */
public abstract class Guard<T> {
    private DependencyResolver resolver;
    private Errors errors = new Errors();

    /**
     * Evaluate target.
     * @param target Target
     * @return true / false
     */
    public abstract boolean isValid(final T target);

    final Guard<T> setResolver(final DependencyResolver resolver) {
        this.resolver = resolver;
        return this;
    }

    protected final <B> B getBean(final Class<B> tClass) {
        return resolver.getBean(tClass);
    }

    protected final Guard<T> addError(final Error.Builder builder) {
        errors.add(builder.build());
        return this;
    }

    protected final Guard<T> addError(final Error error) {
        errors.add(error);
        return this;
    }

    protected Guard<T> addError(final String sourceComponent, final String message, final Throwable t) {
        errors.add(Error.builder()
                .sourceComponent(sourceComponent)
                .message(message)
                .throwable(t)
                .build());

        return this;
    }

    public Errors getErrors() {
        return errors;
    }

    public void setErrors(final Errors errors) {
        this.errors = errors;
    }

    /**
     * Get name of handler.
     *
     * @return Name
     */
    public String getName() {
        return getClass().getSimpleName();
    }

}
