package com.inmeta.fsm;

/**
 * @param <T> Target type
 * @author Alexander Vaagan
 */
public interface Factory<T> {

    /**
     * Create new target instance.
     *
     * @param event Event
     * @return Target instancce
     */
    T createTarget(Event event);
}
