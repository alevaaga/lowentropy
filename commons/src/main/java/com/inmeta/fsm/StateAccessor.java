package com.inmeta.fsm;

/**
 * Interface defining a state accessor.
 * Implementations of this interface should implement methods to get and set the</br>
 * state of the target instance.
 *
 * @param <S> State
 * @param <T> Target
 * @author Alexander Vaagan
 */
public interface StateAccessor<S, T> {

    /**
     * Get state of target.
     *
     * @param target Target
     * @return State
     */
    S getState(final T target);

    /**
     * Set state of target.
     *
     * @param state  State
     * @param target Target
     */
    void setState(S state, T target);
}
