package com.inmeta.lowentropy.application.pilot.mapper;

import com.inmeta.action.SingleResultMapper;
import com.inmeta.lowentropy.application.pilot.dto.PilotDto;
import com.inmeta.lowentropy.domain.pilot.Pilot;

/**
 * @author Alexander Vaagan
 */
public class PilotDtoMapper extends SingleResultMapper<Pilot, PilotDto> {
    private PilotDto dto = new PilotDto();

    @Override
    public SingleResultMapper<Pilot, PilotDto> build(Pilot entity) {
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setTacticalCallsign(entity.getTacticalCallsign());
        return this;
    }

    @Override
    public PilotDto getResult() {
        return dto;
    }
}
