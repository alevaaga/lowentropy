package com.inmeta.lowentropy.application.aircraft.action;

import com.inmeta.action.Action;
import com.inmeta.fsm.InvocationResult;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;
import com.inmeta.lowentropy.domain.aircraft.process.events.StartEngineEvent;

/**
 * @author Alexander Vaagan
 */
public class StartEngine extends Action<Aircraft> {

    @Override
    public Aircraft execute(final Aircraft entity) {

        InvocationResult<Aircraft> invocationResult = process(entity, new StartEngineEvent());

        return entity;
    }



}
