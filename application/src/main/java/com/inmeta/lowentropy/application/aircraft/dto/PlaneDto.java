package com.inmeta.lowentropy.application.aircraft.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Vaagan
 */
public class PlaneDto {
    private Long id;
    private String type;
    private String name;
    private String callsign;

    private String pilotInCommand;

    private String state;
    private Long fuelQuantity;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCallsign() {
        return callsign;
    }

    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    public String getPilotInCommand() {
        return pilotInCommand;
    }

    public void setPilotInCommand(String pilotInCommand) {
        this.pilotInCommand = pilotInCommand;
    }

    public Long getFuelQuantity() {
        return fuelQuantity;
    }

    public void setFuelQuantity(Long fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    @Override
    public String toString() {
        return "PlaneDto : {" +
                "id=" + id +
                ": state='" + state + '\'' +
                ": type='" + type + '\'' +
                ": name='" + name + '\'' +
                ": callsign='" + callsign + '\'' +
                ": pilotInCommand='" + pilotInCommand + '\'' +
                '}';
    }
}
