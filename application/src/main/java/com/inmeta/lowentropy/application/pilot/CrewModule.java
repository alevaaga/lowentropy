package com.inmeta.lowentropy.application.pilot;

import com.inmeta.action.Action;
import com.inmeta.action.EntityBuilder;
import com.inmeta.action.SingleResultMapper;
import com.inmeta.lowentropy.domain.pilot.CrewRepository;
import com.inmeta.lowentropy.domain.pilot.Pilot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Alexander Vaagan
 */
@Component
public class CrewModule {
    @Autowired
    private CrewRepository crewRepository;

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public <T> SingleResultMapper<Pilot, T> create(EntityBuilder<Pilot> factory, SingleResultMapper<Pilot, T> mapper) {
        return mapper.build(crewRepository.save(factory.get()));
    }


    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public <T> SingleResultMapper<Pilot, T> executeAction(Long id, Action<Pilot> action, SingleResultMapper<Pilot, T> mapper) {
        mapper.build(crewRepository.save(action.execute(crewRepository.get(id))));
        mapper.addErrors(action.getErrors());
        return mapper;
    }

}
