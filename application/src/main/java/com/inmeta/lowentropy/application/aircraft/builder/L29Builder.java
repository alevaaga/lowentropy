package com.inmeta.lowentropy.application.aircraft.builder;

import com.inmeta.action.EntityBuilder;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;
import com.inmeta.lowentropy.domain.aircraft.JetFighter;

/**
 * @author Alexander Vaagan
 */
public class L29Builder implements EntityBuilder<Aircraft> {
    private JetFighter fighter;

    public L29Builder() {
        fighter = new JetFighter();
    }


    public L29Builder name(final String name) {
        fighter.setName(name);
        return this;
    }

    public L29Builder callsign(final String name) {
        fighter.setCallsign(name);
        return this;
    }

    public L29Builder type(final String name) {
        fighter.setType(name);
        return this;
    }

    @Override
    public JetFighter get() {
        return fighter;
    }

    public L29Builder fuelCapacity(Long capacity) {
        fighter.setFuelCapacity(capacity);
        return this;
    }
}
