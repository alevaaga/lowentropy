package com.inmeta.lowentropy.application.aircraft.mapper;

import com.inmeta.action.SingleResultMapper;
import com.inmeta.lowentropy.application.aircraft.dto.PlaneDto;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;
import com.inmeta.lowentropy.domain.aircraft.JetFighter;

/**
 * @author Alexander Vaagan
 */
public class PlaneDtoMapper extends SingleResultMapper<Aircraft, PlaneDto> {
    private PlaneDto dto = new PlaneDto();

    @Override
    public SingleResultMapper<Aircraft, PlaneDto> build(final Aircraft entity) {
        JetFighter jet = (JetFighter) entity;

        dto.setId(jet.getId());
        dto.setName(jet.getName());
        dto.setCallsign(jet.getCallsign());
        dto.setType(jet.getType());
        dto.setState(jet.getState().toString());
        dto.setFuelQuantity(jet.getFuelQuantity());

        if(null != jet.getPilot()) {
            dto.setPilotInCommand(jet.getPilot().getName());
        } else if(null != jet.getStudent()) {
            dto.setPilotInCommand(jet.getStudent().getName());
        }

        return this;
    }

    @Override
    public PlaneDto getResult() {
        return dto;
    }
}
