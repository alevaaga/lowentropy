package com.inmeta.lowentropy.application.pilot.dto;

/**
 * @author Alexander Vaagan
 */
public class PilotDto {
    private Long id;
    private String name;
    private String tacticalCallsign;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTacticalCallsign() {
        return tacticalCallsign;
    }

    public void setTacticalCallsign(String tacticalCallsign) {
        this.tacticalCallsign = tacticalCallsign;
    }
}
