package com.inmeta.lowentropy.application.aircraft;

import com.inmeta.action.Action;
import com.inmeta.action.EntityBuilder;
import com.inmeta.action.SingleResultMapper;
import com.inmeta.fsm.DependencyResolver;
import com.inmeta.fsm.Errors;
import com.inmeta.fsm.Event;
import com.inmeta.fsm.EventDispatcher;
import com.inmeta.fsm.InvocationContextFactory;
import com.inmeta.fsm.InvocationResult;
import com.inmeta.fsm.StateMachine;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;
import com.inmeta.lowentropy.domain.aircraft.AircraftRepository;
import com.inmeta.lowentropy.domain.aircraft.AircraftStates;
import com.inmeta.lowentropy.domain.aircraft.process.AircraftInvocationContextFactory;
import com.inmeta.lowentropy.domain.aircraft.process.AircraftStateMap;
import com.inmeta.lowentropy.domain.aircraft.process.ApplicationContextDependencyResolver;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Alexander Vaagan
 */
@Component
public class AircraftModule implements ApplicationContextAware {

    @Autowired
    private AircraftRepository aircraftRepository;

    private DependencyResolver resolver;
    private EventDispatcher<AircraftStates, Aircraft> eventSystem;

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        resolver = new ApplicationContextDependencyResolver(context);
        InvocationContextFactory<AircraftStates, Aircraft> instanceFactory = new AircraftInvocationContextFactory();

        eventSystem = new EventDispatcher<AircraftStates, Aircraft>(
                new StateMachine<AircraftStates, Aircraft>(resolver, instanceFactory)
                        .setMap(new AircraftStateMap(resolver)));
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public <T> SingleResultMapper<Aircraft, T> submitEvent(final Long id, final Event evt, final SingleResultMapper<Aircraft, T> mapper) {
        InvocationResult<Aircraft> invocationResult = eventSystem.process(aircraftRepository.get(id), evt);
        invocationResult.setTarget(aircraftRepository.save(invocationResult.getTarget()));
        mapErrors(mapper, invocationResult.getErrors());
        mapper.build(invocationResult.getTarget());

        return mapper;
    }

    private void mapErrors(SingleResultMapper<Aircraft, ?> mapper, Errors errors) {
        for (com.inmeta.fsm.Error error : errors.getErrors()) {
            mapper.addEerror(String.format("[%s] - %s", error.getSource(), error.getMessage()));
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public <T> SingleResultMapper<Aircraft, T> find(final Long id, final SingleResultMapper<Aircraft, T> mapper) {
        return mapper.build(aircraftRepository.get(id));
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public <T> SingleResultMapper<Aircraft, T> create(final EntityBuilder<Aircraft> entityBuilder, final SingleResultMapper<Aircraft, T> mapper) {
        return mapper.build(aircraftRepository.save(entityBuilder.get()));
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public <T> SingleResultMapper<Aircraft, T> executeAction(Long id, Action<Aircraft> action, SingleResultMapper<Aircraft, T> mapper) {
        mapper.build(aircraftRepository.save(action.setEventSystem(eventSystem).execute(aircraftRepository.get(id))));
        mapper.addErrors(action.getErrors());
        return mapper;
    }

}
