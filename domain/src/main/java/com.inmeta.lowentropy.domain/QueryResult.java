package com.inmeta.lowentropy.domain;

import java.util.List;

/**
 * @author Alexander Vaagan
 */
public class QueryResult<T> {
    private Long hits;
    private List<T> results;

    public Long getHits() {
        return hits;
    }

    void setHits(final Long hits) {
        this.hits = hits;
    }

    public List<T> getResults() {
        return results;
    }

    void setResults(final List<T> results) {
        this.results = results;
    }
}
