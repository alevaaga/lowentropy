package com.inmeta.lowentropy.domain;

import javax.persistence.EntityManager;

/**
 * @author Alexander Vaagan
 */
public abstract class Query<T> {

    public abstract QueryResult<T> find(final EntityManager em);
}
