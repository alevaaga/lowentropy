package com.inmeta.lowentropy.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * @author Alexander Vaagan
 */
@MappedSuperclass
public abstract class DomainObject {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    // Other common fields.


    public final Long getId() {
        return id;
    }

    public final void setId(Long id) {
        this.id = id;
    }
}
