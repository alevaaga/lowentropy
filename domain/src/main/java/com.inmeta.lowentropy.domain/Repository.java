package com.inmeta.lowentropy.domain;

/**
 * @author Alexander Vaagan
 */
public interface Repository<T> {

    T get(final Long id);

    T save(final T entity);

    QueryResult<T> find(final Query<T> query);

}
