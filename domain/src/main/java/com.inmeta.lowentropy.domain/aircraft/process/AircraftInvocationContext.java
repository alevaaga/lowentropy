package com.inmeta.lowentropy.domain.aircraft.process;

import com.inmeta.fsm.InvocationContext;
import com.inmeta.fsm.StateAccessor;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;
import com.inmeta.lowentropy.domain.aircraft.AircraftStates;

/**
 * @author Alexander Vaagan
 */
public class AircraftInvocationContext extends InvocationContext<AircraftStates, Aircraft> {

    @Override
    public StateAccessor<AircraftStates, Aircraft> createStateAccessor() {
        return new AircraftStateAccessor();
    }


    private class AircraftStateAccessor implements StateAccessor<AircraftStates, Aircraft> {

        private AircraftStateAccessor() {}

        @Override
        public AircraftStates getState(Aircraft target) {
            return target.getState();
        }

        @Override
        public void setState(AircraftStates state, Aircraft target) {
            target.setState(state);
        }
    }
}
