package com.inmeta.lowentropy.domain.aircraft.process;

import com.inmeta.fsm.DependencyResolver;
import com.inmeta.fsm.State;
import com.inmeta.fsm.StateMap;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;
import com.inmeta.lowentropy.domain.aircraft.ManeuverState;
import com.inmeta.lowentropy.domain.aircraft.process.guard.EnoughFuelGuard;
import com.inmeta.lowentropy.domain.aircraft.process.handler.maneuvers.InvertedTransitionHandler;
import com.inmeta.lowentropy.domain.aircraft.process.handler.maneuvers.LevelOffTransitionHandler;
import com.inmeta.lowentropy.domain.aircraft.process.handler.maneuvers.ManeuverTransitionHandler;

/**
 * @author Alexander Vaagan
 */
class ManeuverStateMap extends StateMap<ManeuverState, Aircraft> {
    private DependencyResolver resolver;

    public ManeuverStateMap(final DependencyResolver resolver) {
        this.resolver = resolver;
        configureStates();
    }

    private void configureStates() {
        final State<ManeuverState, Aircraft> levelState = addState(
                new State<ManeuverState, Aircraft>(ManeuverState.STRAIGHT_AND_LEVEL)
        );
        final State<ManeuverState, Aircraft> barrelRollState = addState(
                new State<ManeuverState, Aircraft>(ManeuverState.BARRELROLL)
                        .addGuard(new EnoughFuelGuard(400L))
        );
        final State<ManeuverState, Aircraft> invertedState = addState(
                new State<ManeuverState, Aircraft>(ManeuverState.INVERTED)
                        .addGuard(new EnoughFuelGuard(400L))
        );
        final State<ManeuverState, Aircraft> loopState = addState(
                new State<ManeuverState, Aircraft>(ManeuverState.LOOP)
                        .addGuard(new EnoughFuelGuard(400L))
        );
        final State<ManeuverState, Aircraft> rollState = addState(
                new State<ManeuverState, Aircraft>(ManeuverState.ROLL)
                        .addGuard(new EnoughFuelGuard(400L))
        );


        levelState.addTransition(ManeuverState.BARRELROLL, levelState, new ManeuverTransitionHandler());
        levelState.addTransition(ManeuverState.ROLL, levelState, new ManeuverTransitionHandler());
        levelState.addTransition(ManeuverState.LOOP, levelState, new ManeuverTransitionHandler());
        levelState.addTransition(ManeuverState.INVERTED, invertedState, new InvertedTransitionHandler());
        invertedState.addTransition(ManeuverState.STRAIGHT_AND_LEVEL, levelState, new LevelOffTransitionHandler());

    }
}
