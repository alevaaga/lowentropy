package com.inmeta.lowentropy.domain.aircraft.process.guard;

import com.inmeta.fsm.*;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;

/**
 * @author Alexander Vaagan
 */
public class HasPilotGuard extends Guard<Aircraft> {

    @Override
    public boolean isValid(final Aircraft plane) {
        boolean result = null != plane.getPilot();
        if(!result) {
            addError(com.inmeta.fsm.Error.builder().sourceComponent(HasPilotGuard.class.getSimpleName()).message("Aircraft has no pilot.")
                    .build());
        }
        return result;
    }

}
