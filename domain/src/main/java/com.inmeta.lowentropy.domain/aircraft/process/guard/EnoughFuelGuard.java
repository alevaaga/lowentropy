package com.inmeta.lowentropy.domain.aircraft.process.guard;

import com.inmeta.fsm.*;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;

/**
 * @author Alexander Vaagan
 */
public class EnoughFuelGuard extends Guard<Aircraft> {
    private Long limit;

    public EnoughFuelGuard(final Long limit) {
        this.limit = limit;
    }

    @Override
    public boolean isValid(final Aircraft plane) {
        boolean result = plane.getFuelQuantity() != null && plane.getFuelQuantity() > limit;
        if (!result) {
            addError(com.inmeta.fsm.Error.builder().sourceComponent(getClass().getSimpleName())
                    .message("Not enough fuel: " + plane.getFuelQuantity()).build());
        }
        return result;
    }

}
