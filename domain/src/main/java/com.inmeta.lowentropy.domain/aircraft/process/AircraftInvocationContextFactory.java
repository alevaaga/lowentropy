package com.inmeta.lowentropy.domain.aircraft.process;

import com.inmeta.fsm.InvocationContext;
import com.inmeta.fsm.InvocationContextFactory;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;
import com.inmeta.lowentropy.domain.aircraft.AircraftStates;

/**
 * @author Alexander Vaagan
 */
public class AircraftInvocationContextFactory implements InvocationContextFactory<AircraftStates, Aircraft> {

    @Override
    public InvocationContext<AircraftStates, Aircraft> createContext() {
        return new AircraftInvocationContext();
    }
}
