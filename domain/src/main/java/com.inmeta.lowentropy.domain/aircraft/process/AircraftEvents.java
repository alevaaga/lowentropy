package com.inmeta.lowentropy.domain.aircraft.process;

/**
 * @author Alexander Vaagan
 */
public enum AircraftEvents {

    CREATE,

    REFUEL,

    START_ENGINE,

    EMBARK,

    TAKE_OFF,

    LAND, SHUT_DOWN,


}
