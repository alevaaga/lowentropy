package com.inmeta.lowentropy.domain.aircraft.process.handler.maneuvers;

import com.inmeta.fsm.Event;
import com.inmeta.fsm.TransitionHandler;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;

/**
 * @author Alexander Vaagan
 */
public class LevelOffTransitionHandler extends TransitionHandler<Aircraft> {


    @Override
    public Aircraft apply(Event event, Aircraft subject) {
        try {
            System.out.println("Returning to straight and level flight.");
            subject.consumeFuel(50L);
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return subject;
    }
}
