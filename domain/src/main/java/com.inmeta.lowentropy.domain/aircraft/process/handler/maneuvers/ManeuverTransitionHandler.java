package com.inmeta.lowentropy.domain.aircraft.process.handler.maneuvers;

import com.inmeta.fsm.Event;
import com.inmeta.fsm.TransitionHandler;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;

/**
 * @author Alexander Vaagan
 */
public class ManeuverTransitionHandler extends TransitionHandler<Aircraft> {


    @Override
    public Aircraft apply(Event event, Aircraft subject) {
        try {
            System.out.println("Executing a perfect " + event.getEvent().toString().toLowerCase().replace("_", " "));
            subject.consumeFuel(200L);
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return subject;
    }
}
