package com.inmeta.lowentropy.domain.aircraft.process.handler;

import com.inmeta.fsm.Event;
import com.inmeta.fsm.TransitionHandler;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;
import com.inmeta.lowentropy.domain.aircraft.process.events.RefuelEvent;

/**
 * @author Alexander Vaagan
 */
public class RefuelTransitionHandler extends TransitionHandler<Aircraft> {

    @Override
    public Aircraft apply(Event event, Aircraft plane) {
        RefuelEvent refuelEvent = (RefuelEvent) event;
        Long amount = refuelEvent.getAmount();

        Long fuelQuantity = plane.getFuelQuantity() + amount;
        if(fuelQuantity <= plane.getFuelCapacity()) {
            plane.setFuelQuantity(fuelQuantity);
        } else {
            addError(this.getClass().getSimpleName(), "Fuel spill", null);
        }

        return plane;
    }
}
