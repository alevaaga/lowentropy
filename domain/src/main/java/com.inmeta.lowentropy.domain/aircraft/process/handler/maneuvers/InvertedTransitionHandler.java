package com.inmeta.lowentropy.domain.aircraft.process.handler.maneuvers;

import com.inmeta.fsm.Event;
import com.inmeta.fsm.TransitionHandler;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;

/**
 * @author Alexander Vaagan
 */
public class InvertedTransitionHandler extends TransitionHandler<Aircraft> {


    @Override
    public Aircraft apply(Event event, Aircraft subject) {
        try {
            System.out.println("Going inverted!");
            Thread.sleep(200);
            subject.consumeFuel(200L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return subject;
    }
}
