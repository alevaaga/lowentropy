package com.inmeta.lowentropy.domain.aircraft.process.handler;

import com.inmeta.fsm.Event;
import com.inmeta.fsm.TransitionHandler;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;
import com.inmeta.lowentropy.domain.aircraft.process.events.EmbarkPlaneEvent;
import com.inmeta.lowentropy.domain.pilot.CrewRepository;

/**
 * @author Alexander Vaagan
 */
public class EmbarkmentHandler extends TransitionHandler<Aircraft> {
    private CrewRepository crewRepository;

    @Override
    protected void init() {
        crewRepository = getBean(CrewRepository.class);
    }

    @Override
    public Aircraft apply(Event event, Aircraft plane) {
        EmbarkPlaneEvent embarkmentEvent = (EmbarkPlaneEvent) event;
        final Long pilotId = embarkmentEvent.getPilotId();

        if(null == pilotId) {
            addError(this.getClass().getSimpleName(), "No pilot here....", null);
        }

        plane.setPilot(crewRepository.get(pilotId));

        return plane;
    }
}
