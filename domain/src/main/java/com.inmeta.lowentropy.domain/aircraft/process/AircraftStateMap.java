package com.inmeta.lowentropy.domain.aircraft.process;

import com.inmeta.fsm.CompositeState;
import com.inmeta.fsm.DependencyResolver;
import com.inmeta.fsm.InvocationContext;
import com.inmeta.fsm.InvocationContextFactory;
import com.inmeta.fsm.State;
import com.inmeta.fsm.StateMachine;
import com.inmeta.fsm.StateMap;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;
import com.inmeta.lowentropy.domain.aircraft.AircraftStates;
import com.inmeta.lowentropy.domain.aircraft.ManeuverState;
import com.inmeta.lowentropy.domain.aircraft.process.guard.EnoughFuelGuard;
import com.inmeta.lowentropy.domain.aircraft.process.guard.HasPilotGuard;
import com.inmeta.lowentropy.domain.aircraft.process.handler.EmbarkmentHandler;
import com.inmeta.lowentropy.domain.aircraft.process.handler.RefuelTransitionHandler;

/**
 * @author Alexander Vaagan
 */
public class AircraftStateMap extends StateMap<AircraftStates, Aircraft> {
    private DependencyResolver resolver;

    public AircraftStateMap(DependencyResolver resolver) {
        this.resolver = resolver;
        configureStates();
    }

    private void configureStates() {
        final State<AircraftStates, Aircraft> coldState = addState(
                new State<AircraftStates, Aircraft>(AircraftStates.COLD_AND_DARK)
        );

        final State<AircraftStates, Aircraft> runningState = addState(
                new State<AircraftStates, Aircraft>(AircraftStates.RUNNING)
                        .addGuard(new HasPilotGuard())
                        .addGuard(new EnoughFuelGuard(200L))
        );

        final State<AircraftStates, Aircraft> flyingState = addState(
                new CompositeState<AircraftStates, Aircraft>(AircraftStates.FLYING, createFlyingStateMachine())
                        .addGuard(new HasPilotGuard())
                        .addGuard(new EnoughFuelGuard(200L))
        );


        coldState.addTransition(AircraftEvents.START_ENGINE, runningState);
        coldState.addTransition(AircraftEvents.REFUEL, coldState, new RefuelTransitionHandler());
        coldState.addTransition(AircraftEvents.EMBARK, coldState, new EmbarkmentHandler());

        runningState.addTransition(AircraftEvents.TAKE_OFF, flyingState);


        flyingState.addTransition(AircraftEvents.LAND, runningState);

        runningState.addTransition(AircraftEvents.SHUT_DOWN, coldState);

    }

    private StateMachine<?, Aircraft> createFlyingStateMachine() {
        StateMachine<ManeuverState, Aircraft> stateMachine = new StateMachine<ManeuverState, Aircraft>(resolver,
                new InvocationContextFactory<ManeuverState, Aircraft>() {

                    @Override
                    public InvocationContext createContext() {
                        return new ManeuverInvocationContext();
                    }
                });
        stateMachine.setMap(new ManeuverStateMap(resolver));

        return stateMachine;
    }
}
