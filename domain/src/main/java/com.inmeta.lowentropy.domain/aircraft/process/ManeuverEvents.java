package com.inmeta.lowentropy.domain.aircraft.process;

/**
 * @author Alexander Vaagan
 */
public enum ManeuverEvents {

    STRAIGHT_AND_LEVEL,

    INVERTED,

    LOOP,

    ROLL,

    BARRELROLL,
}
