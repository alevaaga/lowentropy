package com.inmeta.lowentropy.domain.aircraft.process.events;

import com.inmeta.fsm.Event;
import com.inmeta.lowentropy.domain.aircraft.process.AircraftEvents;

/**
 * @author Alexander Vaagan
 */
public class EmbarkPlaneEvent extends Event<AircraftEvents> {

    private Long pilotId;

    /**
     * Constructor.
     */
    public EmbarkPlaneEvent(final Long pilotId) {
        super(AircraftEvents.EMBARK);
        this.pilotId = pilotId;
    }

    public Long getPilotId() {
        return pilotId;
    }
}
