package com.inmeta.lowentropy.domain.aircraft.process.events;

import com.inmeta.fsm.Event;
import com.inmeta.lowentropy.domain.aircraft.process.AircraftEvents;

/**
 * @author Alexander Vaagan
 */
public class LandEvent extends Event<AircraftEvents> {

    /**
     * Constructor.
     */
    public LandEvent() {
        super(AircraftEvents.LAND);
    }
}
