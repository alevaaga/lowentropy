package com.inmeta.lowentropy.domain.aircraft.process.events.maneuvers;

import com.inmeta.fsm.Event;
import com.inmeta.lowentropy.domain.aircraft.process.ManeuverEvents;

/**
 * @author Alexander Vaagan
 */
public class RollEvent extends Event<ManeuverEvents> {

    /**
     * Constructor.
     */
    public RollEvent() {
        super(ManeuverEvents.ROLL);
    }
}
