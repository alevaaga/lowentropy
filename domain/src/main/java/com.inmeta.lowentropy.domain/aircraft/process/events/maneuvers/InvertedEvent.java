package com.inmeta.lowentropy.domain.aircraft.process.events.maneuvers;

import com.inmeta.fsm.Event;
import com.inmeta.lowentropy.domain.aircraft.process.ManeuverEvents;

/**
 * @author Alexander Vaagan
 */
public class InvertedEvent extends Event<ManeuverEvents> {

    /**
     * Constructor.
     */
    public InvertedEvent() {
        super(ManeuverEvents.INVERTED);
    }
}
