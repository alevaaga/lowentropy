package com.inmeta.lowentropy.domain.aircraft.process.events.maneuvers;

import com.inmeta.fsm.Event;
import com.inmeta.lowentropy.domain.aircraft.process.ManeuverEvents;

/**
 * @author Alexander Vaagan
 */
public class LoopEvent extends Event<ManeuverEvents> {

    /**
     * Constructor.
     */
    public LoopEvent() {
        super(ManeuverEvents.LOOP);
    }
}
