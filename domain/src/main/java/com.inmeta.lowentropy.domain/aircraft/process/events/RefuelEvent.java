package com.inmeta.lowentropy.domain.aircraft.process.events;

import com.inmeta.fsm.Event;
import com.inmeta.lowentropy.domain.aircraft.process.AircraftEvents;

/**
 * @author Alexander Vaagan
 */
public class RefuelEvent extends Event<AircraftEvents> {

    private Long amount;

    /**
     * Constructor.
     */
    public RefuelEvent(final Long amount) {
        super(AircraftEvents.REFUEL);
        this.amount = amount;
    }

    public final Long getAmount() {
        return amount;
    }
}
