package com.inmeta.lowentropy.domain.aircraft.process.events;

import com.inmeta.fsm.Event;
import com.inmeta.lowentropy.domain.aircraft.PlaneClass;
import com.inmeta.lowentropy.domain.aircraft.process.AircraftEvents;

/**
 * @author Alexander Vaagan
 */
public class CreateJetEvent extends Event<AircraftEvents> {
    private PlaneClass planeClass;

    /**
     * Constructor.
     */
    public CreateJetEvent(final PlaneClass planeClass) {
        super(AircraftEvents.CREATE);
        this.planeClass = planeClass;
    }

    public PlaneClass getPlaneClass() {
        return planeClass;
    }
}
