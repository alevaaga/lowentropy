package com.inmeta.lowentropy.domain.aircraft.process.events;

import com.inmeta.fsm.Event;
import com.inmeta.lowentropy.domain.aircraft.process.AircraftEvents;

/**
 * @author Alexander Vaagan
 */
public class StartEngineEvent extends Event<AircraftEvents> {

    /**
     * Constructor.
     */
    public StartEngineEvent() {
        super(AircraftEvents.START_ENGINE);
    }
}
