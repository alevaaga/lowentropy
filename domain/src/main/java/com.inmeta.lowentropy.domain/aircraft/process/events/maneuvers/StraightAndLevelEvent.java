package com.inmeta.lowentropy.domain.aircraft.process.events.maneuvers;

import com.inmeta.fsm.Event;
import com.inmeta.lowentropy.domain.aircraft.process.ManeuverEvents;

/**
 * @author Alexander Vaagan
 */
public class StraightAndLevelEvent extends Event<ManeuverEvents> {

    /**
     * Constructor.
     */
    public StraightAndLevelEvent() {
        super(ManeuverEvents.STRAIGHT_AND_LEVEL);
    }
}
