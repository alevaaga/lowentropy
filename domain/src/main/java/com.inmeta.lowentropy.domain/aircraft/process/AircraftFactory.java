package com.inmeta.lowentropy.domain.aircraft.process;

import com.inmeta.fsm.Event;
import com.inmeta.fsm.Factory;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;
import com.inmeta.lowentropy.domain.aircraft.JetFighter;
import com.inmeta.lowentropy.domain.aircraft.process.events.CreateJetEvent;

/**
 * @author Alexander Vaagan
 */
public class AircraftFactory implements Factory<Aircraft> {


    @Override
    public Aircraft createTarget(final Event event) {
        if(event instanceof CreateJetEvent) {
            return new JetFighter();
        } else {
            throw new IllegalArgumentException("Unknow event type.");
        }
    }

}
