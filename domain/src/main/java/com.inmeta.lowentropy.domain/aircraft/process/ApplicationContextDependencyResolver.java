package com.inmeta.lowentropy.domain.aircraft.process;

import com.inmeta.fsm.DependencyResolver;
import org.springframework.context.ApplicationContext;

/**
 * @author Alexander Vaagan
 */
public class ApplicationContextDependencyResolver implements DependencyResolver {
    private ApplicationContext context;

    /**
     * Constructor.
     * @param context ApplicationContext
     */
    public ApplicationContextDependencyResolver(final ApplicationContext context) {
        this.context = context;
    }

    @Override
    public <T> T getBean(final Class<T> tClass) {
        return context.getBean(tClass);
    }

    @Override
    public <T> T getBean(final String descriminator, final Class<T> tClass) {
        return context.getBean(descriminator, tClass);
    }
}
