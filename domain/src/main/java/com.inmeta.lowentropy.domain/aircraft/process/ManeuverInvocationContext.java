package com.inmeta.lowentropy.domain.aircraft.process;

import com.inmeta.fsm.InvocationContext;
import com.inmeta.fsm.StateAccessor;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;
import com.inmeta.lowentropy.domain.aircraft.ManeuverState;

/**
 * @author Alexander Vaagan
 */
public class ManeuverInvocationContext extends InvocationContext<ManeuverState, Aircraft> {


    @Override
    public StateAccessor<ManeuverState, Aircraft> createStateAccessor() {
        return new StateAccessor<ManeuverState, Aircraft>() {
            @Override
            public ManeuverState getState(Aircraft target) {
                return target.getManeuverState();
            }

            @Override
            public void setState(ManeuverState state, Aircraft target) {
                target.setManeuverState(state);
            }
        };
    }
}
