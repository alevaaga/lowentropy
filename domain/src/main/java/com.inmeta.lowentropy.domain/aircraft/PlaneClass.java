package com.inmeta.lowentropy.domain.aircraft;

/**
 * @author Alexander Vaagan
 */
public enum PlaneClass {
    SEPL,

    JET,

    TWINN
}
