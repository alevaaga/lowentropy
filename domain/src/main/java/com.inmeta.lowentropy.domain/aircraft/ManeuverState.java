package com.inmeta.lowentropy.domain.aircraft;

/**
 * @author Alexander Vaagan
 */
public enum ManeuverState {

    STRAIGHT_AND_LEVEL,

    INVERTED,

    LOOP,

    ROLL,

    BARRELROLL,
}
