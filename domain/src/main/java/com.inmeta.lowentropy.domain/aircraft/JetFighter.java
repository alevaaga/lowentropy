package com.inmeta.lowentropy.domain.aircraft;

import com.inmeta.lowentropy.domain.pilot.Pilot;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * @author Alexander Vaagan
 */
@Entity
@DiscriminatorValue("JET")
public class JetFighter extends Aircraft {
    @ManyToOne
    private Pilot student;

    public Pilot getStudent() {
        return student;
    }

    public void setStudent(Pilot student) {
        this.student = student;
    }


}
