package com.inmeta.lowentropy.domain.aircraft;

import com.inmeta.lowentropy.domain.DomainObject;
import com.inmeta.lowentropy.domain.pilot.Pilot;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

/**
 * @author Alexander Vaagan
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "planeClass", discriminatorType = DiscriminatorType.STRING)
public abstract class Aircraft extends DomainObject{
    @Enumerated(EnumType.STRING)
    @Column(name = "planeClass", insertable = false, updatable = false)
    private PlaneClass planeClass;

    @Enumerated(EnumType.STRING)
    private AircraftStates state = AircraftStates.COLD_AND_DARK;

    @Enumerated(EnumType.STRING)
    private ManeuverState maneuverState = ManeuverState.STRAIGHT_AND_LEVEL;

    @ManyToOne
    private Pilot pilot;

    private String type;
    private String name;
    private String callsign;
    private Long fuelQuantity = 0L;
    private Long fuelCapacity = 0L;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCallsign() {
        return callsign;
    }

    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    public AircraftStates getState() {
        return state;
    }

    public void setState(AircraftStates state) {
        this.state = state;
    }

    public ManeuverState getManeuverState() {
        return maneuverState;
    }

    public void setManeuverState(ManeuverState maneuverState) {
        this.maneuverState = maneuverState;
    }

    public Long getFuelQuantity() {
        return fuelQuantity;
    }

    public void setFuelQuantity(Long fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    public Long getFuelCapacity() {
        return fuelCapacity;
    }

    public void setFuelCapacity(Long fuelCapacity) {
        this.fuelCapacity = fuelCapacity;
    }

    public void setPilot(Pilot pilot) {
        this.pilot = pilot;
    }

    public Pilot getPilot() {
        return pilot;
    }

    public void consumeFuel(Long quantity) {
        fuelQuantity -= quantity;
    }
}
