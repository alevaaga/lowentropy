package com.inmeta.lowentropy.domain.aircraft;

import com.inmeta.lowentropy.domain.QueryResult;
import com.inmeta.lowentropy.domain.Query;
import com.inmeta.lowentropy.domain.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceContext;

/**
 * @author Alexander Vaagan
 */
@org.springframework.stereotype.Repository
@Transactional(propagation = Propagation.MANDATORY)
public class AircraftRepository implements Repository<Aircraft> {
    @PersistenceContext
    private javax.persistence.EntityManager em;

    @Override
    public Aircraft get(final Long id) {
        return em.find(Aircraft.class, id);
    }

    @Override
    public Aircraft save(final Aircraft entity) {
        em.persist(entity);
        return entity;
    }

    @Override
    public QueryResult<Aircraft> find(final Query<Aircraft> query) {
        return query.find(em);
    }
}
