package com.inmeta.lowentropy.domain.pilot;

import com.inmeta.lowentropy.domain.DomainObject;

import javax.persistence.Entity;

/**
 * @author Alexander Vaagan
 */
@Entity
public class Pilot extends DomainObject {
    private String name;
    private String tacticalCallsign;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTacticalCallsign() {
        return tacticalCallsign;
    }

    public void setTacticalCallsign(String tacticalCallsign) {
        this.tacticalCallsign = tacticalCallsign;
    }
}
