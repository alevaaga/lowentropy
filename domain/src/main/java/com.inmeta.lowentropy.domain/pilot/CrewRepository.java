package com.inmeta.lowentropy.domain.pilot;

import com.inmeta.lowentropy.domain.QueryResult;
import com.inmeta.lowentropy.domain.Query;
import com.inmeta.lowentropy.domain.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceContext;

/**
 * @author Alexander Vaagan
 */
@org.springframework.stereotype.Repository
@Transactional(propagation = Propagation.MANDATORY)
public class CrewRepository implements Repository<Pilot> {

    @PersistenceContext
    private javax.persistence.EntityManager em;

    @Override
    public Pilot get(final Long id) {
        return em.find(Pilot.class, id);
    }

    @Override
    public Pilot save(final Pilot entity) {
        em.persist(entity);
        return entity;
    }

    @Override
    public QueryResult<Pilot> find(final Query<Pilot> query) {
        return query.find(em);
    }
}
