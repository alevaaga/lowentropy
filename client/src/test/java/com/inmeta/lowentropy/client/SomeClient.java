package com.inmeta.lowentropy.client;

import com.inmeta.action.EntityBuilder;
import com.inmeta.action.SingleResultMapper;
import com.inmeta.fsm.NoSuchTransitionException;
import com.inmeta.lowentropy.application.aircraft.AircraftModule;
import com.inmeta.lowentropy.application.aircraft.action.StartEngine;
import com.inmeta.lowentropy.application.aircraft.builder.L29Builder;
import com.inmeta.lowentropy.application.aircraft.dto.PlaneDto;
import com.inmeta.lowentropy.application.aircraft.mapper.PlaneDtoMapper;
import com.inmeta.lowentropy.application.pilot.CrewModule;
import com.inmeta.lowentropy.application.pilot.dto.PilotDto;
import com.inmeta.lowentropy.application.pilot.mapper.PilotDtoMapper;
import com.inmeta.lowentropy.domain.aircraft.Aircraft;
import com.inmeta.lowentropy.domain.aircraft.process.events.EmbarkPlaneEvent;
import com.inmeta.lowentropy.domain.aircraft.process.events.LandEvent;
import com.inmeta.lowentropy.domain.aircraft.process.events.RefuelEvent;
import com.inmeta.lowentropy.domain.aircraft.process.events.TakeOffEvent;
import com.inmeta.lowentropy.domain.aircraft.process.events.maneuvers.BarrelRollEvent;
import com.inmeta.lowentropy.domain.aircraft.process.events.maneuvers.InvertedEvent;
import com.inmeta.lowentropy.domain.aircraft.process.events.maneuvers.LoopEvent;
import com.inmeta.lowentropy.domain.aircraft.process.events.maneuvers.StraightAndLevelEvent;
import com.inmeta.lowentropy.domain.pilot.Pilot;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Alexander Vaagan
 */
public class SomeClient {
    private AircraftModule aircraftModule;
    private CrewModule crewModule;

    @Before
    public void setUp() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        this.aircraftModule = context.getBean(AircraftModule.class);
        this.crewModule = context.getBean(CrewModule.class);
    }

    @Test
    public void runIt() {
        PlaneDto plane = create();

        System.out.println("Initiating startup sequence...");
        startEngine(plane);

        System.out.println("Refueling aircraft...");
        fuelAircraft(plane, 600L);

        PilotDto pilot = createPilot("Alexander Vaagan", "Akhbar");
        System.out.println("Embarking plane...");
        emarkPlane(plane, pilot);

        System.out.println("Initiating startup sequence...");
        startEngine(plane);

        System.out.println("Now we are talking!");
        System.out.println("Taking off!!!");
        takeOff(plane);

        doAirobatics(plane);

        System.out.println("We are finished for today. Landing....");
        land(plane);
    }

    private void doAirobatics(PlaneDto plane) {
        SingleResultMapper<Aircraft, PlaneDto> result = aircraftModule.submitEvent(plane.getId(), new LoopEvent(), new PlaneDtoMapper());
        result = aircraftModule.submitEvent(plane.getId(), new InvertedEvent(), new PlaneDtoMapper());
        try {
            result = aircraftModule.submitEvent(plane.getId(), new LoopEvent(), new PlaneDtoMapper());
        } catch (NoSuchTransitionException e) {
            System.out.println("Ups!");
            result = aircraftModule.submitEvent(plane.getId(), new StraightAndLevelEvent(), new PlaneDtoMapper());
        }
        result = aircraftModule.submitEvent(plane.getId(), new BarrelRollEvent(), new PlaneDtoMapper());
    }

    private void land(PlaneDto plane) {
        SingleResultMapper<Aircraft, PlaneDto> result = aircraftModule.submitEvent(plane.getId(), new LandEvent(), new PlaneDtoMapper());
        assertNotNull("No result", result);
        if(result.hasError()) {
            System.out.println("Landing aborted due to the following errors:");
            printErrors(result.getErrors());
        }
    }

    private void takeOff(PlaneDto plane) {
        SingleResultMapper<Aircraft, PlaneDto> result = aircraftModule.submitEvent(plane.getId(), new TakeOffEvent(), new PlaneDtoMapper());
        assertNotNull("No result", result);
        if(result.hasError()) {
            System.out.println("Take off failed due to the following errors:");
            printErrors(result.getErrors());
        }
    }

    private void emarkPlane(PlaneDto plane, PilotDto pilot) {
        SingleResultMapper<Aircraft, PlaneDto> result = aircraftModule.submitEvent(plane.getId(), new EmbarkPlaneEvent(pilot.getId()), new PlaneDtoMapper());
        assertNotNull("No result", result);
        assertNotNull("No pilot", result.getResult().getPilotInCommand());
        if(result.hasError()) {
            System.out.println("Embarkment failed due to the following errors:");
            printErrors(result.getErrors());
        }
    }

    private PilotDto createPilot(final String name, final String callsign) {
        SingleResultMapper<Pilot, PilotDto> result = crewModule.create(new EntityBuilder<Pilot>() {
            @Override
            public Pilot get() {
                Pilot pilot = new Pilot();
                pilot.setName(name);
                pilot.setTacticalCallsign(callsign);
                return pilot;
            }
        }, new PilotDtoMapper());

        assertNotNull("No result", result);
        assertNotNull("No pilot", result.getResult());
        assertNotNull("Not saved", result.getResult().getId());

        return result.getResult();
    }

    private void fuelAircraft(PlaneDto plane, final Long quantity) {
        SingleResultMapper<Aircraft, PlaneDto> result = aircraftModule.submitEvent(plane.getId(), new RefuelEvent(quantity), new PlaneDtoMapper());
        assertNotNull("No result", result);
        assertNotNull("No plane", result.getResult());
        assertTrue("No fuel", result.getResult().getFuelQuantity() > 200);

        if(result.hasError()) {
            System.out.println("Refueling failed due to the following errors:");
            printErrors(result.getErrors());
        }
    }

    private void startEngine(PlaneDto plane) {
        SingleResultMapper<Aircraft, PlaneDto> result = aircraftModule.executeAction(plane.getId(), new StartEngine(), new PlaneDtoMapper());
        assertNotNull("No result", result);
        assertNotNull("No plane", result.getResult());

        if(result.hasError()) {
            System.out.println("Engine start failed due to the following errors:");
            printErrors(result.getErrors());
        }
    }

    private void printErrors(final List<String> errors) {
        for (String error : errors) {
            System.out.println(error);
        }
    }

    public PlaneDto create() {

        SingleResultMapper<Aircraft, PlaneDto> result = aircraftModule.create(
                new L29Builder()
                        .type("Aero L-29")
                        .callsign("LN-KJJ")
                        .name("Delfin")
                        .fuelCapacity(1200L),
                new PlaneDtoMapper());

        assertNotNull("No result", result);
        assertNotNull("No plane", result.getResult());
        assertNotNull("Not saved", result.getResult().getId());


        return result.getResult();
    }

}
